#!/usr/bin/env python3
"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""
import readline
import os
import sys

import rospy
import rospkg
import rosservice
from vpfr_console.vpfr_instance_info import VpfrInstance
from vpfr_console.vpfr_exception import VpfrException


class VpfrConsole:
    def __init__(self):
        self.__rospack = rospkg.RosPack()
        self.__list_vpfr_instaces = []
        self.__place_holder_txt = ""
        self.__file_loa_regex = r"\w+\.yaml"
        self.__default_config_name = "default_config.yaml"
        self.__selected_algorithm = None
        self.__current_instance = None
        self.__algorithm_list = []
        self._update_data()

    def command_executor(self, command):  # fehler abfangen
        if command == "select-instance":
            self._update_data()
            print("active vpfr-instances: ")
            instance_number = 1
            if not self.__list_vpfr_instaces:
                print("Error: No running vpfr instance")
                return
            for i in self.__list_vpfr_instaces:
                i.update()
                instace_name = i.get_service_pre_address()
                print(str(instance_number) + " " + str(instace_name))
                instance_number = instance_number + 1
            instance_number = int(input("select a vpfr-instance number: "))
            try:
                self.__current_instance = self.__list_vpfr_instaces[
                    int(instance_number) - 1
                ]
            except Error:
                print("Instance entered is invalid!")
        elif command == "update-instances":
            self._update_data()
            print("updated")  # error
        elif command == "set-subscriber-topic":
            subscriber_name = input("enter a subscriber name: ")
            self._set_subscriber_channel(subscriber_name)
        elif command == "update-instances":
            self._scan_vpfr_instances()
            print("scanned for instances")
        elif command == "set-publisher-topic":
            publisher_name = input("enter a publisher name: ")
            self._set_publisher_channel(publisher_name)
        elif command == "get-current-channels":
            self._get_channel_info()
        elif command == "set-additional-data":
            add_data = input("set additional data: ")
            self._set_additional_info(add_data)
        elif command == "get-additional-data":
            self._get_additional_data()
        elif command == "get-algorithms":
            self._print_algorithms()
        elif command == "set-algorithm":
            algo = input("Enter Algorithm Name: ")
            if algo is not None:
                self._set_algorithm(algo)
            else:
                print("Name not valid!")
        elif command == "help" or command == "?":
            self._print_help()
        elif command == "exit":
            sys.exit(0)
        else:
            print("Error: Command entered does not exist!")
            self._print_help()
            return

    def _print_help(self):
        opts = [
            "select-instance",
            "update-instances",
            "set-subscriber-topic",
            "set-publisher-topic",
            "get-current-channels",
            "set-additional-data",
            "get-additional-data",
            "get-algorithms",
            "set-algorithm",
            "get-current-algorithm",
            "exit",
        ]
        print("Help Menu\n")
        print("Commands:")
        for i in opts:
            print(str(i))

    def _update_data(self):
        self.__list_vpfr_instaces = ""
        self.__list_vpfr_instaces = self._scan_vpfr_instances()

    def _scan_vpfr_instances(self):
        list_of_vpfr = []
        service_list = rosservice.get_service_list()
        for service in service_list:
            if service.startswith("/vpfr/"):  # scan all vpfr services
                vpfr_service_data = service.split("/")
                vpfr_type = vpfr_service_data[2]  # type name
                vpfr_instance = vpfr_service_data[3]  # instace name
                found = False
                for check_instance in list_of_vpfr:
                    if (
                        check_instance.get_vpfr_type() == vpfr_type
                        and check_instance.get_vpfr_instance() == vpfr_instance
                    ):
                        found = True
                        break
                if not found:
                    list_of_vpfr.append(VpfrInstance(vpfr_type, vpfr_instance))
        return list_of_vpfr

    def _update_instance(self):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        try:
            self.__current_instance.update()
        except rospy.ServiceException as exc:
            print(
                "(vpfr-console) Service did not process request: " + str(exc)
            )
        except VpfrException as exc:
            print(
                "(vpfr-console) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )

    def _get_channel_info(self):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        print(
            "subscriber channel: "
            + str(self.__current_instance.get_subscriber_channel())
        )
        print(
            "subscriber channel data type: "
            + str(self.__current_instance.get_subscriber_channel_type())
        )
        print(
            "publisher channel: "
            + str(self.__current_instance.get_publisher_channel())
        )
        print(
            "publisher channel data type: "
            + str(self.__current_instance.get_publisher_channel_type())
        )

    def _set_subscriber_channel(self, subscriber_name):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        try:
            self.__current_instance.set_subscriber_channel(subscriber_name)
            self._update_instance()
        except rospy.ServiceException as exc:
            print(
                "(vpfr-console) Service did not process request: " + str(exc)
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-console) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )

    def _set_publisher_channel(self, publisher_name):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        print("(vpfr-console) set publisher channel")
        try:
            self.__current_instance.set_publisher_channel(publisher_name)
            self._update_instance()
        except rospy.ServiceException as exc:
            print(
                "(vpfr-console) Service did not process request: " + str(exc)
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-console) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )

    def _set_algorithm(self, algorithm_name):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        self.__current_instance.update()
        self._get_algorithms()
        contains_algo = False
        for a in self.__algorithm_list:
            if algorithm_name == a.get_name():
                contains_algo = True
                break
        if not contains_algo:
            print("Error: Algorithm not in list!")
            return
        sel_algorithm = None
        for algorithm in self.__current_instance.get_algorithms():
            if algorithm.is_enabled():
                if algorithm_name == algorithm.get_name():
                    sel_algorithm = algorithm
                    break

        print(
            "(vpfr-console) set algorithm ('" + sel_algorithm.get_name() + "')"
        )
        if not sel_algorithm.is_current():
            try:
                sel_algorithm.set_current()
                self._update_instance()
            except rospy.ServiceException as exc:
                print(
                    "(vpfr-console) Service did not process request: "
                    + str(exc)
                )
                self._show_alert(
                    QMessageBox.Critical,
                    "Service did not process request",
                    str(exc),
                )
                self._update_data()
            except VpfrException as exc:
                print(
                    "(vpfr-console) Service ('"
                    + exc.getPlace()
                    + "') call was unsuccessful because '"
                    + exc.getMessage()
                    + "'"
                )

    def _get_algorithms(self):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        self.__current_instance.update()
        self.__current_instance.scan_algorithm()
        self.__algorithm_list = []
        self.__algorithm_list = self.__current_instance.get_algorithms()

    def _print_algorithms(self):
        self._get_algorithms()
        for a in self.__algorithm_list:
            print(a.get_name())

    def _set_additional_info(self, data_array):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        if data_array is not None:
            self.__current_instance.set_additional_data(data_array)
            return
        print("Error: additional data given is empty!")

    def _get_additional_data(self):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        print(str(self.__current_instance._get_additional_data()))

    def _export_loa(self, file_name):
        if self.__current_instance is None:
            print("Error: instance is not set!")
            return
        if file_name is None:
            print("Error: file name given is empty!")
            return
        self.__current_instance.export_loa_file(file_name)


def main():
    vpfr_console = VpfrConsole()
    rospy.init_node("VPFR_Console_Node", anonymous=True)
    while 1:
        try:
            vpfr_console.command_executor(input("> "))
        except KeyboardInterrupt:
            sys.exit()


def command_completer(text, state):
    options = [x for x in opts if x.startswith(text)]
    try:
        return options[state]
    except IndexError:
        return None


if __name__ == "__main__":
    opts_commands = [
        "select-instance",
        "update-instances",
        "set-subscriber-topic",
        "set-publisher-topic",
        "get-current-channels",
        "set-additional-data",
        "get-additional-data",
        "get-algorithms",
        "set-algorithm",
        "get-current-algorithm",
        "exit",
    ]
    opts = opts_commands

    readline.set_completer(command_completer)
    readline.parse_and_bind("tab: complete")
    main()
