"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

from vpfr.srv import *
import rospy
from vpfr_console.vpfr_exception import VpfrException


class VpfrAlgorithmInfo:
    def __init__(
        self,
        instance,
        name,
        version,
        authors,
        license,
        main_class,
        main_file,
        loader,
        configs,
        enabled,
        current
    ):
        self.__instance = instance
        self.__name = name
        self.__version = version
        self.__authors = authors
        self.__license = license
        self.__main_class = main_class
        self.__main_file = main_file
        self.__loader = loader
        self.__configs = configs
        self.__enabled = enabled
        self.__current = current

    def get_name(self):
        return self.__name

    def get_version(self):
        return self.__version

    def get_authors(self):
        return self.__authors

    def get_license(self):
        return self.__license

    def get_main_class(self):
        return self.__main_class

    def get_main_file(self):
        return self.__main_file

    def get_loader(self):
        return self.__loader

    def get_configs(self):
        return self.__configs

    def is_enabled(self):
        return self.__enabled

    def is_current(self):
        return self.__current

    def get_current_config(self):
        for config in self.__configs:
            if config.is_current():
                return config
        return None

    def set_current(self):
        set_algorithm = rospy.ServiceProxy(
            self.__instance.get_service_pre_address() + "set_algorithm",
            AlgorithmSet
        )
        data = set_algorithm(self.__name)
        if not data.success:
            raise VpfrException(
                self.__instance.get_service_pre_address() + "set_algorithm",
                data.message
            )
