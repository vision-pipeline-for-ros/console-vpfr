"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

class VpfrException(Exception):
    def __init__(self, loc, message):
        self.__loc = loc
        self.__message = message

    def get_message(self):
        return self.__message

    def get_loc(self):
        return self.__Loc
