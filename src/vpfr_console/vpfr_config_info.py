"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

from vpfr.srv import *
import rospy
from vpfr_qtgui.vpfrexception import VpfrException


class AlgorithmConfig:
    def __init__(self, instance, algorithm_name, name, path, current):
        self.__instance = instance
        self.__algorithm_name = algorithm_name
        self.__name = name
        self.__path = path
        self.__current = current

    def get_name(self):
        return self.__name

    def get_path(self):
        return self.__path

    def is_current(self):
        return self.__current

    def use_config(self):
        set_algorithm_config = rospy.ServiceProxy(
            self.__instance.get_service_pre_address() + "set_algorithm_config",
            DataConfig,
        )
        data = set_algorithm_config(self.__algorithm_name, self.__name)
        if not data.success:
            raise VpfrException(
                self.__instance.get_service_pre_address()
                + "set_algorithm_config",
                data.message,
            )
