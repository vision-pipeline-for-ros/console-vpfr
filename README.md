![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)


# Project pages

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---


# Console VPFR
This project is for controlling the vision pipeline via a console interface.
With this plugin, information from the individual instances can be read and manipulated through the vision pipeline.  




Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/console-vpfr/-/wikis/home)
